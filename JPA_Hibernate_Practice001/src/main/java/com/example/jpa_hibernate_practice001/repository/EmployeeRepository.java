package com.example.jpa_hibernate_practice001.repository;

import com.example.jpa_hibernate_practice001.model.Employee;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class EmployeeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Employee save(Employee emp) {
        entityManager.persist(emp);
        return emp;
    }
    public Employee findById(long id) {
        Employee emp = (Employee) entityManager.find(Employee.class, id);
        return emp;
    }
    @Transactional
    public Employee update(Employee emp) {
        entityManager.merge(emp);
        return emp;
    }
    @Transactional
    public Employee deleteById(long id) {
        Employee emp = findById(id);
        if (emp != null) {
            entityManager.remove(emp);
        }
        return emp;
    }
    @Transactional
    public int deleteAll() {
        Query query = entityManager.createQuery("DELETE FROM Employee ");
        return query.executeUpdate();
    }
    public List<Employee> findAll(){
        TypedQuery<Employee> query = entityManager.createQuery("SELECT e FROM Employee e",Employee.class);
        return query.getResultList();
    }
}
