package com.example.jpa_hibernate_practice001;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaHibernatePractice001Application {

    public static void main(String[] args) {
        SpringApplication.run(JpaHibernatePractice001Application.class, args);
    }

}
