package com.example.jpa_hibernate_practice001.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String gender;
    private String email;

    public Employee(String name, String gender, String email) {
        this.name = name;
        this.gender = gender;
        this.email = email;
    }

    public Employee(long id, String name, String gender, String email) {
        this.id=id;
        this.name = name;
        this.gender = gender;
        this.email = email;
    }
}
