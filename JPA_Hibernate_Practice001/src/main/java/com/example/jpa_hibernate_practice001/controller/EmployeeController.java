package com.example.jpa_hibernate_practice001.controller;

import com.example.jpa_hibernate_practice001.model.Employee;
import com.example.jpa_hibernate_practice001.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeRepository empRepository;


    @GetMapping("/getAll")
    public List<Employee> getAllEmployee(){
       return empRepository.findAll();
    }
    @GetMapping("/getEmpById/{id}")
    public Employee getById(@PathVariable Long id){
        return empRepository.findById(id);
    }
    @PostMapping("/addNewEmp")
    public Employee insertEmployee(@RequestParam String name, String gender, String email){
       return empRepository.save(new Employee(name,gender,email));
    }

    @DeleteMapping("/deleteEmp/{id}")
    public Employee deleteEmployee(@PathVariable long id){
        return empRepository.deleteById(id);
    }

    @PutMapping("/updateEmpById/{id}")
    public Employee updateEmp(@PathVariable long id,@RequestParam String name,@RequestParam String gender,@RequestParam String email){
        return empRepository.update(new Employee(id, name,gender,email));
    }




}
